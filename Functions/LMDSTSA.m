function x_hat =  LMDSTSA(y,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs Multidimensional Speech Enhancement in STFT domain.
%
%
% example call:
% xhat = MultiDimensionalSpeechEnhancement(y, 'x', x, 'Rx', (ntime,nfreq,nfreq), 'Rd', (ntime,nfreq,nfreq), 'Estimator', 'MagnitudeWienerFilter', 'SpeechCovEstimator', 'NMF', 'NoisePSDEstimator', 'oracle')
%
% or e.g.:
% xhat = MultiDimensionalSpeechEnhancement(y, 'x', x, 'Estimator', 'Plourde', 'SpeechCovEstimator', 'Plourde', 'NoisePSDEstimator', 'oracle')
%
% INPUT:
% y ... noisy speech
%
% OUTPUT:
% x_hat ... enhanced speech
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 14.01.2019
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (1) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = LMDSTSA_InitParams(); % Set parameters according to [1]
[~, p, ~, ~] = parseParams_johannes(p,varargin{:}); % set parameters according to function call

%%%%%%%%%%%%%%%%%%% (2) STFT: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = p.framelength*p.fs; % framelength in samples
[y_framed, ~] = FramingAndWindowing(y,p);
fftlength = N*p.FFT_factor;
nbins = fftlength/2+1;
Y = fft(y_framed,fftlength); 
Y = Y(1:(fftlength/2+1),:); % half sided stft
NrOfFrames = size(y_framed,2);

% if we do some oracle-scenario experiment
if strcmp(p.SpeechCovEstimator,'oracle')
    [x_framed, ~] = FramingAndWindowing(p.x,p);
    X = fft(x_framed,fftlength); % half sided stft
    X = X(1:(fftlength/2+1),:);
end

if (strcmp(p.NoisePSDEstimator,'oracle') || strcmp(p.NoisePSDEstimator,'FirstNFrames'))
    [d_framed, ~] = FramingAndWindowing(y-p.x,p);
    D = fft(d_framed,fftlength); % half sided stft
    D = D(1:(fftlength/2+1),:);
else
    D_p = zeros(size(Y));
end

%%%%%%%%%%%%%%%%%%% (3) Speech Estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init estimation:
G = zeros(size(Y));
Rd_l = 0;
Ry_l = diag(diag(Y(:,1)*Y(:,1)'));
R_absy_l = diag(diag(abs(Y(:,1))*abs(Y(:,1)')));
                    
Rx_l = zeros(size(Ry_l));
R_absx_l = zeros(size(Ry_l));
                                        
mu_absd_l = abs(Y(:,1));
mu_absx_l = zeros(size(mu_absd_l));
                    
                    
% parameters for noise PSD estimation [3]
hop_noise = p.frameshift;
pH1 = 1/2; % a priori spp
pH0 = 1-pH1;
pfac = pH0./pH1; % a priori probability ratio
xi_H1 = 10^(15/10); % "typical a priori SRN", section IV-D [3]
GH1 = xi_H1./(1+xi_H1); 
p_bar_l_1 = 1; % for avoiding stagnation
alpha_pow = exp(-p.frameshift/0.0717); %0.8, 0.8944 % noise psd estimation
alpha_prob = exp(-p.frameshift/0.152); %0.9, 0.9487 % noise psd estimation


W = fft(p.window(N));
nbins_mlw = (sum(10*log10(abs(W)/abs(W(1))) > -10) - 1)/2;
CovMat_mlw = convmtx([ones(nbins_mlw*2+1,1).*exp(1i*[-2*pi/fftlength; 0; 2*pi/fftlength]*(N-1)/2); zeros(nbins - (nbins_mlw*2+1),1)], nbins + 1);

% A simple decision directed loop [5]:

hop_ratio = hop_noise/p.frameshift;% ratio of hopsize of noise psd estimator to hopsize of the speech estimator

Xhat_l = zeros(nbins,1);
R_absy_DD = 0;
Rx_unsmoothed_l = 0;
Rabsx_unsmoothed_l = 0;
reg_fac = 0.04;
for FrameIndex = 1:NrOfFrames
    Y_l = Y(:,FrameIndex); % read noise speech frame
    Y_p_l = Y_l*Y_l'; % compute instantaneous power
    
    if FrameIndex == 1; Ry_l = Y_p_l; else; Ry_l = Ry_l*p.alpha_DD + Y_p_l*(1-p.alpha_DD); end % compute PSD/Covariance estimate
    
    % (1) Noise PSD/Covariance estimation 
	switch p.NoisePSDEstimator
        case 'mmse'
        Y_pow_l = diag(Y_p_l);
        if FrameIndex == 1
            D_p_l = Y_pow_l;
        end
        if ~mod((FrameIndex-1),hop_ratio)
            pH1_y = 1./(1 + pfac.*(1+xi_H1).*exp(-(Y_pow_l ./D_p_l).*GH1)); % eq. (18) [3]
            p_bar_l = alpha_prob.*p_bar_l_1 + (1-alpha_prob).*pH1_y; % eq. (23) [3]
            % avoiding stagnation, Section IV-C:
            pH1_y = min(pH1_y,1-0.01*(p_bar_l>0.99));
            cond_exp_noise_l = (1 - pH1_y).*Y_pow_l  + pH1_y.*D_p_l;
            D_p_l =  alpha_pow.*D_p_l + (1-alpha_pow).*cond_exp_noise_l; % eq. (8) [3]
            Rd_l = real(diag(D_p_l)); % noise covariance matrix
            R_absd = abs(Rd_l);
            D_p(:,FrameIndex) = D_p_l;
        end
        case 'oracle'
        Rd_l = alpha_pow*Rd_l + (1-alpha_pow)*D(:,FrameIndex)*D(:,FrameIndex)';
        if FrameIndex==1
            R_absd = abs(D(:,FrameIndex))*abs(D(:,FrameIndex))';
        else
            R_absd = alpha_pow*R_absd + (1-alpha_pow)*abs(D(:,FrameIndex))*abs(D(:,FrameIndex)');
        end
        
        case 'FirstNFrames'
        if FrameIndex == 1
            if isempty(p.Nnoise)
                Nnoise = NrOfFrames;
            else
                Nnoise = p.Nnoise;%
            end
            
            Rd_l = D(:,1:Nnoise)*D(:,1:Nnoise)'/Nnoise;
            R_absd = abs(D(:,1:Nnoise))*abs(D(:,1:Nnoise)')/Nnoise;
            
            
            
        end
    end
    
    % (2) Speech PSD/Covariance estimation 
    if ~mod((FrameIndex-1),hop_ratio)
        switch p.SpeechCovEstimator
            case 'oracle'
               
                Rx_ml = X(:,FrameIndex)*X(:,FrameIndex)';
                Rabsx_ml = abs(X(:,FrameIndex))*abs(X(:,FrameIndex)');
               
                Rx_l = p.alpha_DD*Rx_unsmoothed_l+(1-p.alpha_DD)*Rx_ml;
                R_absx_l = p.alpha_DD*Rabsx_unsmoothed_l +(1-p.alpha_DD)*Rabsx_ml; 
                
                
                Ry_l = Rx_l + Rd_l;
                Ry_l = Ry_l + reg_fac*trace(Ry_l)*eye(nbins)/nbins;  
                 
                alpha_mix = 0.1;
                
                R_absy_l = abs(R_absx_l + R_absd );
                R_absy_l = R_absy_l + reg_fac*trace(R_absy_l)*eye(nbins)/nbins;  
              
                R_absxy_l = alpha_mix*(R_absx_l) + (1-alpha_mix)*(diag(diag(abs(R_absx_l)))) ;%(R_absx_l) + ;% + mux_ml*mud_ml'
                
                
            case 'Plourde' % soft thresholded structured, eq. (46) in Plourde2011 
                tv = 3500; % crosses per second
                tu = 6000; % crosses per second
                ZCR = sum(abs(diff(sign(y_framed(:,FrameIndex))))>0)/p.framelength;
                delta_i = (ZCR<=tv)*1 + ((ZCR<tu) & (ZCR>tv))*(tu-ZCR)/(tu-tv) + 0*(ZCR>=tu);
               % delta_i = 1;
                Rx_ml = Ry_l-Rd_l; 
                Rx_ml(diag(diag(Rx_ml)) < 0) = 0; % apply threshold 
                Rx_l = p.alpha_DD*Rx_unsmoothed_l+(1-p.alpha_DD)*Rx_ml; % Update a priori SNR
               
                
                Rx_l = delta_i*Rx_l + (1-delta_i)*diag(diag(Rx_l));
                Ry_l = Rx_l + Rd_l;  
                Ry_l = Ry_l + reg_fac*trace(Ry_l)*eye(nbins)/nbins;    
            case 'MagnitudeDD'
                
                %MagnitudeDD:
                alpha_DD_y = 0.9;
                
                
             
                
                Ry_l = alpha_DD_y*Ry_l + (1-alpha_DD_y)*(Y_l)*Y_l'; %
                
                alpha_DD_y = 0.998;
                R_absy_DD = alpha_DD_y*R_absy_DD + (1-alpha_DD_y)*abs(Y_l)*abs(Y_l'); 
                R_absy_l = R_absy_DD;
                R_absy_l = R_absy_l + reg_fac*trace(R_absy_l)*eye(nbins)/nbins;     
                
                
                Rx_ml = diag(diag(Ry_l-Rd_l));
                Rx_ml(Rx_ml < 0) = 0; % apply threshold 
                Rx_ml(1,:) = 0; Rx_ml(:,1) = 0; Rx_ml(end,:) = 0; Rx_ml(:,end) = 0;
                Rx_l = Rx_ml;%max(p.alpha_DD*Rx_unsmoothed_l+(1-p.alpha_DD)*Rx_ml,p.xi_min*Rd_l); %  (1)
                   
                Xhat_prelim = diag(Rx_ml)./diag(Rx_ml +Rd_l).*Y_l;
                R_absx_ml = abs(Xhat_prelim)*abs(Xhat_prelim.');
                    
                alpha_DD_absx = 0.5;
                R_absx_l = max(alpha_DD_absx*R_absx_l+(1-alpha_DD_absx)*R_absx_ml,p.xi_min*Rd_l); % (3) R_absx_ml.*Mask;% R_absx_l
                Rx_absx_l(1,:) = 0; Rx_absx_l(:,1) = 0; Rx_absx_l(end,:) = 0; Rx_absx_l(:,end) = 0;
                                            
                X_hat_prel = sqrt(abs(diag(Rx_l)));%abs(Xhat_l);%max(G(:,FrameIndex-1).*abs(Y_l),0);% %PREV: *sqrt(pi/2);%diag(Rx_l)./diag(Rx_l + Rd_l).*Y_l;
                D_hat_prel= sqrt(abs(diag(Rd_l)));%;% PREV: *sqrt(pi/2);
                mu_absd_l = p.alpha_DD*mu_absd_l + (1-p.alpha_DD)*D_hat_prel; % (5)
                mu_absx_l = p.alpha_DD*mu_absx_l + (1-p.alpha_DD)*abs(X_hat_prel); % (6) 
                    
                mu_ad = mu_absx_l*mu_absd_l.';
              

                % compute upper bounds:
                R_absxy_up = R_absx_l + mu_ad;
                
                % compute lower bounds:
                R_absxy_lo = abs(Rx_l);
                
                % compute crosscovariance matrices
                mixing_facxy = 0.5;%delta_i; 
                R_absxy_l = (mixing_facxy*R_absxy_up + (1-mixing_facxy)*R_absxy_lo);
            
        end
    end
                
                
    % (3) Speech estimation 
    switch p.Estimator
        case 'WienerFilter'
          
            %G_l = Rx_l/;%.*exp(-1i*angle(Y_l*Y_l'));
            G_l = Rx_l/Ry_l;
            Xhat_l = G_l*Y_l;
            Xhat_l = abs(Xhat_l).*exp(1i*angle(Y_l));
            
        case 'WienerSTSA'
            G_l = Rx_l/Ry_l;
            Xhat_l = abs(G_l*Y_l).*exp(1i*angle(Y_l));
            
        case 'Plourde'
         %   G_l = Rx_l/Ry_l;%diag(diag(Rd_l));%Ry_l;
            G_l = Rx_l/Ry_l;%.*exp(1i*2*pi*rand(size(Rx_l)));

            lambda = 0.5; 
            Xhat_l = sqrt(abs(G_l*Y_l).^2 + lambda*diag(G_l*Rd_l)).*exp(1i*angle(Y_l)); % eq. (37) in Plourde2011
            
        case 'MagnitudeWienerFilter' % This is our proposal
            %G_l = R_absxy_l/R_absy_l;
            %Xhat_l = (G_l*abs(Y_l)).*exp(1i*angle(Y_l));
            
            
            G_l = ((R_absxy_l)/R_absy_l)/nbins*2;%G_l = diag(diag(R_absxy./R_absy));
            % G_l = abs(Rx_l)/abs(Ry_l);
            Xhat_l = G_l*abs(Y_l).*exp(1i*angle(Y_l));%abs(G_l*Y_l).*exp(1i*angle(Y_l));%
            G_l  = Xhat_l./Y_l;
            G_l(abs(G_l)<p.G_min) = p.G_min;
            G_l(abs(G_l)>p.G_max) = p.G_max*exp(1i*angle(G_l(abs(G_l)>p.G_max)));

            % apply gain:
           % X_hat = Y_l.*G_l; 
    end
    
    
   
   Rx_unsmoothed_l = diag(diag(Xhat_l*Xhat_l'));
   Rabsx_unsmoothed_l = abs(Xhat_l)*abs(Xhat_l');%diag(diag(abs(Xhat_l)*abs(Xhat_l')));
   
   mux_unsmoothed_l = abs(Xhat_l);
   mud_unsmoothed_l = abs(Y_l - Xhat_l);
   
   G(:,FrameIndex)  = Xhat_l./Y_l;
    

end

G(abs(G)<p.G_min) = p.G_min;
G(abs(G)>p.G_max) = p.G_max*exp(1i*angle(G(abs(G)>p.G_max)));

% apply gain:
if strcmp(p.ReconstructionPhase,'noisy')
    X_hat = Y.*abs(G); 
else
    X_hat = Y.*G;
end

%%%%%%%%%%%%%%%%%%% (7) Signal Synthesis: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_hat_frames = rifft(X_hat,N*p.FFT_factor); % restore full dft
x_hat = STFT_OLA(x_hat_frames,p);
x_hat = [x_hat(1:min(length(y),length(x_hat))); zeros(max(length(y)-length(x_hat),0),1)]; % ensure correct length

end

function param = LMDSTSA_InitParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializes parameters for MultiDimensionalSpeechEnhancement. If MultiDimensionalSpeechEnhancement is called with different
% parameter settings, the default values are overwritten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
param = struct( 'x', [], ... % clean input signal
                'Estimator', 'MagnitudeWienerFilter', ... % choose which gain to apply for innovation process estimation
                'framelength', 0.032, ...   % time domain frame length, needs to be consistent with PYTHON
                'frameshift', 0.008, ...    % sliding window shift, needs to be consistent with PYTHON
                'fs',16e3,... % sampling frequency, needs to be consistent with PYTHON
                'G_min', 0, ... % maximum noise suppression
                'G_max', inf, ... % maximum noise suppression
                'alpha_DD', 0.98,... % smoothing constant for speech variance, needs to be consistent with PYTHON
                'window', @(N) sqrt(hamming(N,'periodic')), ... % chosen window function, needs to be consistent with PYTHON
                'xi_ml_min', 0, ... % defines minimum ratio of speech variance ml estimate and noise variance 
                'xi_min', 0, ... % defines minimum ratio of speech and noise variance 
                'FFT_factor', 1, ...
                'NoisePSDEstimator', 'mmse', ...
                'SpeechCovEstimator' , 'MagnitudeDD', ... %'oracle', ...
                'ReconstructionPhase', 'noisy',...
                'NoiseName', '', ...
                'Nnoise', [], ...
                'Rx', [], ...
                'Rd', [] ...
                );
end