Demo_LMDSTSA.m demonstrates the linear multidimensional short-time spectral amplitude estimator (LMDSTSA) [2].

figure2.m reproduces figure 2 from [1].

[1] J. Stahl, Sean U.N. Wood, P. Mowlaee "Overcoming Covariance Matrix Phase Sensitivity in Single-Channel Speech Enhancement with Correlated Spectral Components", in Proc. ITG Symposium on Speech Communication, Oct 2018, pp. 286–290.

[2] J. Stahl, S. Wood, and P. Mowlaee, “Single-channel speech enhancement with correlated spectral components: Limits - potential,” submitted to IEEE/ACM Trans. Audio, Speech, and Language Processing, 2018.


