%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure2.m reproduces figure 2 from [1].
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 25.05.2018
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl, Sean U.N. Wood, P. Mowlaee "Overcoming Covariance Matrix Phase Sensitivity
%     in Single-Channel Speech Enhancement with Correlated Spectral Components", 
%     submitted to 13th ITG Symposium on Speech Communication 2018.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     figure2.m reproduces figure 2 from [1].
%     Copyright (C) 2018 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all

addpath('./Functions/')
rng default  % For reproducibility

% set parameters according to [1]
nsamples = 1e5;
SpeechCorrelationCoeff = 0.99*exp(1i*pi/4);
NoiseCorrelationCoeff = 0*exp(-1i*3*pi/4);

PhaseErrorList = 0:(pi/16):(2*pi); NrOfPhaseErrors = length(PhaseErrorList);
SNRlist = -10:10:10; NrOfSNRs = length(SNRlist);

GenerateCircularComplexSamples = @(variance, nsamples) sqrt(variance(:,ones(1,nsamples))/2).*(randn(length(variance),nsamples) + 1i*randn(length(variance),nsamples));

MSE_complex_MDWF = zeros(NrOfPhaseErrors,NrOfSNRs);
MSE_complex_MDSTSA = zeros(NrOfPhaseErrors,NrOfSNRs);
MSE_complex_LMDSTSA = zeros(NrOfPhaseErrors,NrOfSNRs);

MSE_mag_MDWF = zeros(NrOfPhaseErrors,NrOfSNRs);
MSE_mag_MDSTSA = zeros(NrOfPhaseErrors,NrOfSNRs);
MSE_mag_LMDSTSA = zeros(NrOfPhaseErrors,NrOfSNRs);

lambda_LMDSTSA = 0.5;


for SNRCounter = 1:NrOfSNRs
    
    % (1) generate data
    % generate "speech"
    CovSpeech = [1 SpeechCorrelationCoeff; conj(SpeechCorrelationCoeff) 1];
    x = complex_mvnrnd([0; 0], CovSpeech, nsamples).';

    % generate "noise"
    CovNoise = [1 NoiseCorrelationCoeff; conj(NoiseCorrelationCoeff) 1];
    d = complex_mvnrnd([0; 0], CovNoise, nsamples).';

    % normalize data to desired power levels:
    pow_x = sum(sum(abs(x.^2)));
    pow_d = sum(sum(abs(d.^2)));
    x2d_pow_ratio = 10^(SNRlist(SNRCounter)/10);
    x_norm = x/sqrt(pow_x);
    d_norm = d/sqrt(pow_d);
    d = d_norm/sqrt(x2d_pow_ratio);
    x = x_norm;

    % normalize covariance matrices to power
    CovNoise = conj(CovNoise/pow_d)/x2d_pow_ratio;
    CovNoise_hat = d*d'/nsamples;
    CovSpeech = conj(CovSpeech)/pow_x;
    CovSpeech_hat = x*x'/nsamples;
            
    % generate observation:
	y = x+d;
    
    % (2) get sample covariance matrix and optain LMD-STSA gain
    CovSpeech_mag_hat = abs(x)*abs(x')/nsamples;
    CovSpeechObs_mag_hat = abs(x)*abs(y')/nsamples;
    CovObs_hat = abs(y)*abs(y')/nsamples;
    
    G_LMDSTSA = CovSpeechObs_mag_hat/CovObs_hat;
	xhat_LMDSTSA = (G_LMDSTSA.'*abs(y)).*exp(1i*angle(y));
    
    G_WF = diag(diag(CovSpeech)./diag((CovSpeech+CovNoise)));
    xhat_WF = G_WF*y;
    
    for PhaseErrorCounter = 1:NrOfPhaseErrors
            % generate filters and apply them:
            PhaseError = PhaseErrorList(PhaseErrorCounter)*rand(1,nsamples)-PhaseErrorList(PhaseErrorCounter)/2; 
            xhat_MDWF = zeros(2,nsamples);
            xhat_MDSTSA = zeros(2,nsamples);
     
            % generate estimates:
            for kk = 1:nsamples
                CovSpeech_hat = CovSpeech.*exp(1i*[0 PhaseError(kk); -PhaseError(kk) 0]);
                G_hat = CovSpeech_hat/(CovSpeech_hat+CovNoise);
                
                xhat_MDWF(:,kk) = G_hat*y(:,kk);
                xhat_MDSTSA(:,kk) =sqrt(abs(G_hat*y(:,kk)).^2 + lambda_LMDSTSA*diag(G_hat*CovNoise)).*exp(1i*angle(y(:,kk)));% %
            end
            
            MSE_complex_MDWF(PhaseErrorCounter,SNRCounter) = 10*log10(sum(sum(abs(x-xhat_MDWF).^2,1))./sum(sum(abs(x-xhat_WF).^2,1)));
            MSE_complex_MDSTSA(PhaseErrorCounter,SNRCounter) = 10*log10(sum(sum(abs(x-xhat_MDSTSA).^2,1))./sum(sum(abs(x-xhat_WF).^2,1)));
            MSE_complex_LMDSTSA(PhaseErrorCounter,SNRCounter) = 10*log10(sum(sum(abs(x-xhat_LMDSTSA).^2,1))./sum(sum(abs(x-xhat_WF).^2,1)));
            MSE_mag_MDWF(PhaseErrorCounter,SNRCounter) = 10*log10(sum(sum(abs(abs(x)-abs(xhat_MDWF)).^2,1))./sum(sum(abs(abs(x)-abs(xhat_WF)).^2,1)));
            MSE_mag_MDSTSA(PhaseErrorCounter,SNRCounter) = 10*log10(sum(sum(abs(abs(x)-abs(xhat_MDSTSA)).^2,1))./sum(sum(abs(abs(x)-abs(xhat_WF)).^2,1)));
            MSE_mag_LMDSTSA(PhaseErrorCounter,SNRCounter) = 10*log10(sum(sum(abs(abs(x)-abs(xhat_LMDSTSA)).^2,1))./sum(sum(abs(abs(x)-abs(xhat_WF)).^2,1)));
    end
end

%%%% some plotting:
Color1 = [0 0 0]; Color2 = [0.4 0.4 0.4]; Color3 = [0.8 0.2 0]; linewidth = 1;

figure
subplot(231)
hold on
plot(PhaseErrorList,MSE_complex_MDWF(:,1),'Color',Color1,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_complex_MDSTSA(:,1),'Color',Color2,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_complex_LMDSTSA(:,1),'Color',Color3,'LineWidth',linewidth)
axis square; axis tight
box on; ylabel('MSE(x)')
title(['SNR = ' num2str(SNRlist(1)) ' dB'])
ylim([-3 5]) 

subplot(234)
hold on
plot(PhaseErrorList,MSE_mag_MDWF(:,1),'Color',Color1,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_mag_MDSTSA(:,1),'Color',Color2,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_mag_LMDSTSA(:,1),'Color',Color3,'LineWidth',linewidth)
axis square; axis tight
box on; xlabel('Phase Error'); ylabel('MSE(|x|)')
ylim([-5 4])

subplot(232)
hold on
plot(PhaseErrorList,MSE_complex_MDWF(:,2),'Color',Color1,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_complex_MDSTSA(:,2),'Color',Color2,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_complex_LMDSTSA(:,2),'Color',Color3,'LineWidth',linewidth)
axis square; axis tight
box on; 
title(['SNR = ' num2str(SNRlist(2)) ' dB'])
ylim([-3 5])

subplot(235)
hold on
plot(PhaseErrorList,MSE_mag_MDWF(:,2),'Color',Color1,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_mag_MDSTSA(:,2),'Color',Color2,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_mag_LMDSTSA(:,2),'Color',Color3,'LineWidth',linewidth)
axis square; axis tight
box on; xlabel('Phase Error'); 
ylim([-5 4])

subplot(233)
hold on
plot(PhaseErrorList,MSE_complex_MDWF(:,3),'Color',Color1,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_complex_MDSTSA(:,3),'Color',Color2,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_complex_LMDSTSA(:,3),'Color',Color3,'LineWidth',linewidth)
axis square; axis tight
box on; 
title(['SNR = ' num2str(SNRlist(3)) ' dB'])
ylim([-3 5])
subplot(236)
hold on
plot(PhaseErrorList,MSE_mag_MDWF(:,3),'Color',Color1,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_mag_MDSTSA(:,3),'Color',Color2,'LineWidth',linewidth)
plot(PhaseErrorList,MSE_mag_LMDSTSA(:,3),'Color',Color3,'LineWidth',linewidth)
axis square; axis tight
box on; xlabel('Phase Error'); 
ylim([-5 4])

