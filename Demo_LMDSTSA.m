function Demo_LMDSTSA()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demo_LMDSTSA.m demonstrates the linear multidimensional short-time spectral amplitude estimator (LMDSTSA) [2].
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 14.01.2019
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl, Sean U.N. Wood, P. Mowlaee "Overcoming Covariance Matrix Phase Sensitivity
%     in Single-Channel Speech Enhancement with Correlated Spectral Components", 
%     in Proc. ITG Symposium on Speech Communication, Oct 2018, pp. 286–290.
% [2] J. Stahl, S. Wood, and P. Mowlaee, “Single-channel speech enhancement with
%     correlated spectral components: Limits - potential,” submitted to IEEE/ACM
%     Trans. Audio, Speech, and Language Processing, 2018.
% [3] Y. Ephraim and D. Malah, “Speech enhancement using a minimum-mean square
%     error short-time spectral amplitude estimator,” IEEE Trans. Acoust., Speech, Sig-
%     nal Process., vol. 32, no. 6, pp. 1109–1121, Dec 1984.
% [4] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S. Pallett, and N. L.
%     Dahlgren, “DARPA TIMIT acoustic phonetic continuous speech corpus CDROM,”
%     1993. [Online]. Available: http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [5] A. Varga, H. J. M. Steeneken, M. Tomlinson, and D. Jones, “The NOISEX–92
%     Study on the Effect of Additive Noise on Automatic Speech Recognition,” Tech-
%     nical Report, DRA Speech Research Unit, 1992.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Demo_LMDSTSA.m demonstrates the speech enhancement algorithm proposed in [2].
%     Copyright (C) 2019 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) add paths
addpath('ExternalFunctions/')
addpath('Functions/')
addpath('Audio/')

% (2) load audio: 
[y, fs] = audioread('noisy_factory1_16k_10dB_ID24.wav'); % [4] & [5]
[x, ~] = audioread('clean_ID24.wav'); % [4] 

% (3) enhance speech signal using the algorithm from [2]
xhat_LMDSTSA = LMDSTSA(y,'Gmin', 0.1, ...
                                     'framelength', 0.032, ...
                                     'frameshift', 8e-3...
                                     );

% (4) enhance speech signal using the algorithm from [3]
xhat_STSA = STFT_SpeechEnhancement(y,'Estimator','MMSESTSA', ...
                                     'Gmin', 0.1, ...
                                     'framelength', 0.032, ...
                                     'frameshift', 8e-3...
                                     );

% (5) plot and play the audiosignals
y = audiosignal(y);
x = audiosignal(x);
xhat_LMDSTSA = audiosignal(xhat_LMDSTSA);
xhat_STSA = audiosignal(xhat_STSA);

figure
subplot(221)
x.plot_spectrogram; caxis([-80 -30]); colorbar; title('Clean')
subplot(222)
y.plot_spectrogram; caxis([-80 -30]); colorbar; title('Noisy')
subplot(223)
xhat_STSA.plot_spectrogram; caxis([-80 -30]); colorbar; title('STSA')
subplot(224)
xhat_LMDSTSA.plot_spectrogram; caxis([-80 -30]); colorbar; title('LMDSTSA')

y.play; pause(y.siglength)
xhat_STSA.play; pause(xhat_STSA.siglength)
xhat_LMDSTSA.play
end
